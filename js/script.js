'use strict';

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get name() {
        return this._name;        
    }

    set name(newName) {
        newName = newName.trim();
        if(newName === '') {
            console.log("Ім'я не може бути порожнім");
        } else {
        this._name = newName; 
        }
    }

    get age() {
        return this._age;        
    }

    set age(newAge) {
        if(newAge > 0) {
            this._age = newAge;
        } else {
        console.log("Введено некоректне значення");
        }
    }

    get salary() {
        return this._salary;        
    }

    set salary(newSalary) {
        if(newSalary >= 0) {
            this._salary = newSalary;
        } else {
        console.log("Введено некоректне значення");
        }
    }

}

const employee1 = new Employee('Roman', 25, 5000);
// console.log(employee1.name); 
employee1.name = 'Denis'
console.log(employee1.name); 

// console.log(employee1.age); 
employee1.age = 42;
console.log(employee1.age);

// console.log(employee1.salary); 
employee1.salary = 7000;
console.log(employee1.salary); 


class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get salary() {
        return this._salary*3;        
    }

    set salary(newSalary) {
        if(newSalary >= 0) {
            this._salary = newSalary;
        } else {
        console.log("Введено некоректне значення");
        }
    }
    
}

const programmer1 = new Programmer('Anna', 22, 5000, 'English');
console.log(programmer1);
const programmer2 = new Programmer('Petro', 52, 10000, 'German');
console.log(programmer2);
const programmer3 = new Programmer('Andy', 43, 8000, 'Ukrainian');
console.log(programmer3);
const programmer4 = new Programmer('Olya', 38, 3000, 'French');
console.log(programmer4);


